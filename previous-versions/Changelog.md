# IDN-Toolbox

The IDN-Toolbox is a software for an IDN consumer, which is able to visualize incoming IDN streams (both IDN continuous and discrete graphics mode, aka wave and frame mode) on a computer screen.

## Change Log / History:

- Version of April 2020: still available as file <IDN-Toolbox-Lite_Windows_2020-04.zip>

The [April 2020] version implements IDN Service Discovery, which is covered in the (Draft) IDN Hello Protocol Specification.

- Version of October 2019: still available as file <IDN-Toolbox_2019-10.zip>

The [October 2019] version of IDN-Toolbox is not yet supporting the feature of IDN Service Discovery, which is covered in the (Draft) IDN Hello Protocol Specification. 
To send IDN streams to the IDN-Toolbox, the IP address of the computer running IDN-Toolbox needs to be known to the IDN producer.

For experimenting in the IDN-NPP, LaProMo is pre-configured to send the IDN stream to 127.0.0.1 (loopback address). When LaProMo and IDN-Toolbox are running
on the same computer, everything is fine!

[Outdated] **2-do:** Implement IDN Service Discovery into IDN-Toolbox. Planned for Q1/2020.
