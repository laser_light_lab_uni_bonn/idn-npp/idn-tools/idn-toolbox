# IDN-Toolbox, folder "previous-versions"

**IMPORTANT: The zip/portable files in this folder are OUTDATED! Please read the update note below!**

The IDN-Toolbox is a software for an IDN consumer, which is able to visualize incoming IDN streams (both IDN continuous and discrete graphics mode, aka wave and frame mode) on a computer screen.

## IMPORTANT Update January 15, 2022:

Since July 2021, a comprehensive documentation of the multi-channel IDN-Toolbox is available as a YouTube video:
https://www.youtube.com/watch?v=Jj_wTgCwtMA&t=472s

Portable versions will be updated from time to time and are available for download to use on Windows (64bit + 32bit versions), Linux (Ubuntu) and Mac OS (restricted operation depending on graphics cards):
https://uni-bonn.sciebo.de/s/NyOy2lMwThRpwXZ

The previous portable (Windows only) versions of the IDN-Toolbox are still available in this repository (in this folder).
