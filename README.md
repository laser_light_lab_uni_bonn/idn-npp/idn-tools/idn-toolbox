# IDN-Toolbox

The IDN-Toolbox is a software for an IDN consumer, which is able to visualize incoming IDN streams (both IDN continuous and discrete graphics mode, aka wave and frame mode) on a computer screen.

## IMPORTANT Update January 15, 2022:

Since July 2021, a comprehensive documentation of the multi-channel IDN-Toolbox is available as a YouTube video:
https://www.youtube.com/watch?v=Jj_wTgCwtMA&t=472s

Portable versions will be updated from time to time and are available for download to use on Windows (64bit + 32bit versions), Linux (Ubuntu) and Mac OS (restricted operation depending on graphics cards):
https://uni-bonn.sciebo.de/s/NyOy2lMwThRpwXZ

The previous portable (Windows only) versions of the IDN-Toolbox are still available in this repository.


## Update April 10, 2020: file <IDN-Toolbox-Lite_Windows.zip> (Portable Windows Binary)

This version implements IDN Service Discovery, which is covered in the (Draft) IDN Hello Protocol Specification.

The IDN-Toolbox will respond to IDN Scan Requests and IDN Service Map Requests with some default information, and thus will be avaiable in the
network in a plug & play fashion. The IDN service information may be manually adapted using the configuration file <libidn-stream.conf>.

See file <libidn-stream-README.conf> for inline explanations. It is possible to configure the IDN-Toolbox to offer several IDN-Hello
laser (projector) services.

## Open issues:

For experimenting in the IDN-NPP, LaProMo is pre-configured to send the IDN stream to 127.0.0.1 (loopback address). When LaProMo and IDN-Toolbox are running
on the same computer, everything is fine! With the current version implementing the IDN Service Discovery, the IDN-Toolbox maybe found twice by LaProMo.

**2-do:** currently nothing (important) ... stay tuned for feature updates! (planned for Q2/2020)

E.g. MacOS portable binary version (currently in experimental stage), Linux portable binary version (maybe not that much needed)

## Credits:

The IDN-Toolbox project was initially started by the student **Achim Sieg** of the University of Bonn. Achim finished the first implementation phase in his Bachelor thesis in August 2017.

The IDN-Toolbox received the First Place Fenning Award for ILDA Digital Network Standards Technical Achievement at the 2018 ILDA Conference in Montreal, Canada:

### ILDA Awards booklet: https://www.ilda.com/resources/Awards/2018-Awards-Booklet-03.pdf [pages 58-59 ]

### ILDA Awards video: https://www.youtube.com/watch?v=wtFpct7eMiU

The April 2020 version of the IDN-Toolbox was updated by students **Georg Kuhlemann** und **Sebastian Tasch** in a Bachelor project course 
(so called German "Projektgruppe Kommunikationssysteme") in the winter term 2019/2020.
